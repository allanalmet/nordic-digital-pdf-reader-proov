<?php 

namespace App;

use Exception;
use Smalot\PdfParser\Parser;

class PdfReader 
{

    private $_parsedPdf;

    public function __construct($pdf)
    {
        if (!file_exists($pdf)) 
        {
            throw new Exception("Pdf file not found!!");
        } 
        $this->_parsedPdf = (new Parser())->parseFile($pdf);
    }

    public function getData() 
    {
        echo "<pre>";
        $pages = $this->_parsedPdf->getPages()[0];
        $hasData = true;
        $items = [];

        $descriptionX = 5350.769 + 25;
        $descriptionY = -13909.678 - 400;
        $barcodeX = 5534.129 + 15;
        $barcodeY = -14493.838 - 400;
        $quantityX = 5801.008 + 20;
        $quantityY = -15077.998 - 400;
        $priceX = 6407.728 + 30;
        $priceY = -16266.958 - 400;
        $sumX = 8238.685; - 500;
        $sumY = -18644.878 - 50;


        $barcodeRowXError = 50;
        $barcodeRowYError = 2400;

        $quantityRowXError = 140;
        $quantityRowYError = 2400;

        $priceRowXError = 30;
        $priceRowYError = 2400;
        $descriptionRowXError = 30;
        $descriptionRowYError = 2400;


        //ROW ASTE

        $descriptionRowX = 2948;
        $descriptionRowY = 5500;

        $barcodeRowX = 2944;
        $barcodeRowY = 5500;

        $quantityRowX = 2944;
        $quantityRowY = 5500;

        $priceRowX = 2944;
        $priceRowY = 5400;

        $sumRowX = 2946;
        $sumRowY = 5400;


        

        while($hasData) 
        {

            $name = $this->getCellData($pages->getTextXY($descriptionX, $descriptionY, $descriptionRowXError, $descriptionRowYError), true);
            $quantity = $this->getCellData($pages->getTextXY($quantityX, $quantityY, $quantityRowXError, $quantityRowYError));
            $price = $this->getCellData($pages->getTextXY($priceX, $priceY, $priceRowXError, $priceRowYError));

            
            if ($name == null) 
            {
                $hasData = false;
                break;
            }
            
            $items[] = [
                "ean" => $this->getCellData($pages->getTextXY($barcodeX, $barcodeY, $barcodeRowXError, $barcodeRowYError)),
                "quantity" => $quantity,//$this->getCellData($pages->getTextXY($quantityX, $quantityY, $quantityRowXError, $quantityRowYError)),
                "price" => $price,//$this->getCellData($pages->getTextXY($priceX, $priceY, $priceRowXError, $priceRowYError)),
                "sum" => $quantity * floatval(str_replace(",", ".", $price)),//$this->getCellData($pages->getTextXY($sumX, $sumY, $sumRowXError, $sumRowYError)),
                "name" => $name,
            ];

            $descriptionX += $descriptionRowX;
            $descriptionY -= $descriptionRowY;
            $barcodeX += $barcodeRowX;
            $barcodeY -= $barcodeRowY;
            $quantityX += $quantityRowX;
            $quantityY -= $quantityRowY;
            $priceX += $priceRowX;
            $priceY -= $priceRowY;
            $sumX += $sumRowX;
            $sumY -= $sumRowY;

        }

        print_r([
            "invoiceId" => $pages->getDataTm()[8][1],
            "items" => $items
        ]);

        exit;
    }

    public function getCellData(array $datas, $space = false) 
    {
        $data_string = "";
        foreach ($datas as $data) {
            $data_string .= $data[1];
            if($space){
                $data_string .= " ";
            }
        } 
        return trim($data_string);
    }

}